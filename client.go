package mongo_client

import "gitlab.com/nariflix/libraries/mongo-client/model"

var properties *model.Database

func MongoProperties() *model.Database {
	if properties == nil {
		properties = &model.Database{}
	}
	return properties
}
