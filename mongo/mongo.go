package mongo

import (
	"context"
	"fmt"
	"gitlab.com/nariflix/libraries/mongo-client/model"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

var dbClient *mongo.Client
var databaseProperties model.Database

func initClient() *mongo.Client {

	log.Println("Inicialicando cliente de conexão com a base de dados.")

	ctx := context.Background()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(databaseProperties.Url))

	if err != nil {
		log.Fatal(err)
	}

	if err := client.Ping(ctx, nil); err != nil {
		log.Fatal("falha ao estabelecer conexão com a base de dados: ", err)
	}

	log.Println("Conexão com a base de dados estabelecida com sucesso!")

	return client
}

func InitMongoClient(properties model.Database) {
	databaseProperties = properties
	dbClient = initClient()
}

func GetMongoConnection() *mongo.Database {

	if dbClient == nil {
		fmt.Println("Cliente deve ser inicializado. chame o metodo InitMongoClient antes de tentar obter uma conexão.")
		return nil
	}

	return dbClient.Database(databaseProperties.Name)
}

func FinalizeMongoClient() error {
	fmt.Println("Desconectando do SGBD")
	return dbClient.Disconnect(context.Background())
}
