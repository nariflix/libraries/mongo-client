package model

type Database struct {
	Name       string
	Url        string
	Collection string
}

func (database *Database) New() *Database {
	return &Database{}
}
